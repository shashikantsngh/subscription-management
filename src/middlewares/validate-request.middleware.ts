import { NextFunction, Request, Response } from "express";

const validateRequest = (req: Request, res: Response, next: NextFunction, schema: any) => {
    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true // remove unknown props
    };
    const { error, value } = schema.validate(req.body, options);
    if (error) {
        // next(`Validation error: ${error.details.map(x => x.message).join(', ')}`);

        res.status(400).json({ message: `Validation error: ${error.details.map(x => x.message).join(', ')}` });

    } else {
        req.body = value;
        next();
    }
}

export = validateRequest;