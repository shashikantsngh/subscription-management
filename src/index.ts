import express from "express";
import { sequelize } from "./db/db.init";
import bodyparser from "body-parser";
import cors from "cors";

const app = express();
app.use(bodyparser.urlencoded({ extended: false }));
app.use(bodyparser.json());
app.use(cors());

app.listen(5000, async () => {
    sequelize.authenticate().then(async () => {
        console.log("connected");
        try {
            await sequelize.sync();
        } catch (err) {
            console.log(err);
        }
    });
});

