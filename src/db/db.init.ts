import { Sequelize } from "sequelize-typescript";
import environments from "../utils/config";

export const sequelize = new Sequelize(`${environments.DB_NAME}`, `${environments.DB_USERNAME}`, `${environments.DB_PASSWORD}`, {
    host: environments.HOST_NAME,
    port: environments.PORT_NAME,
    dialect: "mysql",
    models: [`${__dirname}/../app/models`]
});