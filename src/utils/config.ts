import * as dotenv from "dotenv";

dotenv.config();
let path;
switch (process.env.NODE_ENV) {
    case "test":
        path = `${__dirname}/../../.env.test`;
        break;
    case "production":
        path = `${__dirname}/../../.env.prod`;
        break;
    default:
        path = `${__dirname}/../../.env.dev`;
}
console.log("-------->>", path);
dotenv.config({ path: path });

const HOST_NAME = process.env.HOST_NAME;
const PORT_NAME = Number(process.env.PORT_NAME);
const DB_PASSWORD = process.env.DB_PASSWORD;
const DB_NAME = process.env.DB_NAME;
const DB_USERNAME = process.env.DB_USERNAME;

export default { HOST_NAME, PORT_NAME, DB_PASSWORD, DB_NAME, DB_USERNAME };
